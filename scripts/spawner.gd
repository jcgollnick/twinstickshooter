extends Spatial

export(float) var RADIUS = 50.0
export(float) var SPREAD = 30.0
export(float) var FORCE = 15.0
export(float) var MIN_FORCE = 5.0
export(float) var SPAWN_RATE = 0.2
export(int)   var MAX_COUNT = 500
var _count := 0

export(Resource) var spawn_resource
onready var spawn_scn : PackedScene = load(spawn_resource.resource_path)

var timer = Timer.new()
func _ready():
  add_child(timer)
  timer.wait_time = SPAWN_RATE
  timer.one_shot = false
  timer.connect("timeout", self, "make_spawn")
  timer.start()


func make_spawn():
  if _count >= MAX_COUNT:
    return

  var pos : Vector3 = Vector3(randf()-0.5, 0.0, randf()-0.5).normalized() * RADIUS + global_transform.origin
  var spread : Vector3 = Vector3(randf()-0.5, 0.0, randf()-0.5) * lerp(0.0, SPREAD, randf())
  var spawn : RigidBody = spawn_scn.instance()
  var impulse : Vector3 = ((global_transform.origin + spread) - pos).normalized() * lerp(MIN_FORCE, FORCE, randf())
  var torque  : Vector3 = Vector3(randf(),randf(),randf()).normalized() * lerp(0.0, MIN_FORCE, randf())

  get_tree().root.add_child(spawn)
  spawn.global_transform.origin = pos
  spawn.apply_impulse(Vector3.ZERO, impulse * spawn.mass)
  spawn.apply_torque_impulse(torque)
  spawn.connect("tree_exited", self, "_on_Spawn_despawn")

  _count += 1


func _on_Spawn_despawn():
  _count -= 1
